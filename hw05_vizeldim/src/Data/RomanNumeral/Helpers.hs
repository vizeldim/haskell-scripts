module Data.RomanNumeral.Helpers where

import Data.Tuple (swap)

-- these are to help with RomanNumerals, feel free to edit it if needed
-- you don't have to use them at all... but you can

-- see: http://www.csgnetwork.com/csgromancnv.html

maximal = 4999
minimal = -4999

zero = "" -- zero is very special value
negativePrefix = '-'
messageBadIntegral integral = "Cannot convert to Roman Numeral: '" ++ show integral ++ "'"
messageBadNumeral numeral = "Illegal Roman Numeral: '" ++ numeral ++ "'"

intToChar :: Integral a => [(a, [Char])]
intToChar = [ ( 1, "I")
            , ( 4, "IV")
            , ( 5, "V")
            , ( 9, "IX")
            , ( 10, "X")
            , ( 40, "XL")
            , ( 50, "L")
            , ( 90, "XC")
            , ( 100, "C")
            , ( 400, "CD")
            , ( 500, "D")
            , ( 900, "CM")
            , ( 1000, "M")
            ]

charToInt :: Integral a => [([Char], a)]
charToInt = map swap intToChar

intToCharRev :: Integral a => [(a, [Char])]
intToCharRev = reverse intToChar

findNearestMaxRoman :: (Integral n, Show n) => n -> (n, [Char])
findNearestMaxRoman n = findNearestMaxRomanAux n intToCharRev

findNearestMaxRomanAux :: Integral n => n -> [(n, [Char])] -> (n, [Char])
findNearestMaxRomanAux n ((value, numeral):xs)
    | value <= n = (value, numeral)
    | otherwise = findNearestMaxRomanAux n xs
findNearestMaxRomanAux _ [] = (0, "0")
