module Data.ResultList where

import Data.List (sort)
import Data.Semigroup

data ResultList a = Results [a] | Error Int String
                  deriving (Show, Read, Eq)

-- | Extract results to a list or return error with message
toList :: ResultList a -> [a]
toList (Results lst) = lst
toList (Error code msg) = error ("#" ++ show code ++ ": " ++ msg)


instance Semigroup (ResultList a) where
  -- | Merge two result lists together
  (<>) (Results lst1) (Results lst2) = Results (lst1 ++ lst2)
  (<>) (Results lst) (Error code msg) = Error code msg
  (<>) (Error code msg) (Results lst) = Error code msg
  (<>) (Error code1 msg1) (Error code2 msg2) = 
      if code2 > code1 
        then (Error code2 msg2)
        else (Error code1 msg1)

instance Monoid (ResultList a) where
  mempty = Results []
  mappend = (<>)

instance Functor ResultList where
  -- | Apply function over sorted list
  fmap f (Results lst) = Results (fmap f lst)
  fmap _ (Error code msg) = Error code msg

instance Applicative ResultList where
  pure x = Results [x]
  -- | Apply all functions to elements in result list   
  (Results funcsLst) <*> (Results elemsLst) = Results (funcsLst <*> elemsLst)
  (Error code msg) <*> _ = Error code msg
  _ <*> (Error code msg) = Error code msg

instance Monad ResultList where
  -- | Apply on result list if valid and not empty
  (Results lst) >>= func = foldr (<>) mempty (map func lst)
  (Error code msg) >>= _ = Error code msg
