module Data.Shapes where

--------------------------------------------------------------------------------
-- DO NOT CHANGE DATA TYPES DEFINITIONS

newtype Circle = Circle { ciRadius :: Double }
               deriving (Show, Read, Eq)

data Triangle = EquilateralTriangle { etSide :: Double }
              | IsoscelesTriangle { itBase :: Double, itLeg :: Double }
              | ScaleneTriangle { stSideA :: Double, stSideB :: Double, stSideC :: Double }
              deriving (Show, Read, Eq)

data Quadrilateral = Square { sqSide :: Double}
                   | Rectangle { reSideA :: Double, reSideB :: Double }
                   deriving (Show, Read, Eq)

--------------------------------------------------------------------------------

class Validable a where
  valid :: a -> Bool
  valid = undefined

-- Instances for each type to check validity by `valid` function
instance Validable Circle where
  valid (Circle r) = r > 0

instance Validable Triangle where
  valid (EquilateralTriangle s) = s > 0
  valid (IsoscelesTriangle b l) = b > 0 && l > 0 && b <= 2*l
  valid (ScaleneTriangle a b c) = a > 0 && b > 0 && c > 0
                                  && a+b > c && b+c > a && c+a > b

instance Validable Quadrilateral where
  valid (Square s) = s > 0
  valid (Rectangle a b) = a > 0 && b > 0

-- Typeclass for 2D shapes (subclass of Validable)
class Validable a => Shape2D a where
  area :: a -> Double
  circumference :: a -> Double


-- Instances for the types to compute circumference and area
instance Shape2D Circle where
  area (Circle r) = if valid (Circle r) then pi * r * r else 0

  circumference (Circle r) = if valid (Circle r) then 2 * pi * r else 0

instance Shape2D Quadrilateral where
  area (Square s) = if valid (Square s) then s * s else 0
  area (Rectangle a b) = if valid (Rectangle a b) then a * b else 0
  circumference (Square s) = if valid (Square s) then 4 * s else 0
  circumference (Rectangle a b) = if valid (Rectangle a b) then 2 * (a + b) else 0

instance Shape2D Triangle where
  area (EquilateralTriangle s) = if valid (EquilateralTriangle s)
                                   then (sqrt 3 / 4) * s * s
                                   else 0

  area (IsoscelesTriangle b l) = if valid (IsoscelesTriangle b l)
                                    then let h = sqrt (l * l - ( b / 2 ) * ( b / 2 ))
                                         in b * h / 2
                                    else 0

  area (ScaleneTriangle a b c) = if valid (ScaleneTriangle a b c)
                                   then let s = (a + b + c) / 2
                                            in sqrt ( s * ( s - a ) * ( s - b ) * ( s - c ))
                                   else 0

  circumference (EquilateralTriangle s) = if valid (EquilateralTriangle s)
                                             then 3 * s
                                             else 0

  circumference (IsoscelesTriangle b l) = if valid (IsoscelesTriangle b l)
                                            then b + 2 * l
                                            else 0

  circumference (ScaleneTriangle a b c) = if valid (ScaleneTriangle a b c)
                                            then a + b + c
                                            else 0
