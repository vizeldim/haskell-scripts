module Data.RomanNumeral where

import Data.Maybe (fromMaybe)

-- Use Data.RomanNumeral.Helpers submodule
-- In case of need, feel free to change or enhance Helpers or create own
-- submodule
--
-- DO NOT HARDCODE ANY STRINGs/CHARs IN THIS MODULE!
import qualified Data.RomanNumeral.Helpers as Helpers

-- | RomanNumeral type (wrapper) for English numerals
newtype RomanNumeral = RomanNumeral String
                     deriving (Show, Read)

-- | Pack Integer into RomanNumeral (English numeral string)
pack :: (Integral a, Show a) => a -> RomanNumeral
pack integral = RomanNumeral $ fromMaybe err (integral2RomanNumeral integral)
              where err = error $ Helpers.messageBadIntegral integral

-- | Unpack RomanNumeral (English numeral string) to Integer
unpack :: RomanNumeral -> Integer
unpack (RomanNumeral numeral) = fromMaybe err (romanNumeral2Integral numeral)
    where err = error $ Helpers.messageBadNumeral numeral


-- | Translate Integral value to Roman Numeral String (if possible)
integral2RomanNumeral :: (Integral a, Show a) => a -> Maybe String
integral2RomanNumeral x
    | x < fromIntegral Helpers.minimal || x > fromIntegral Helpers.maximal = Nothing
    | x < 0 = Just $ [Helpers.negativePrefix] ++ integral2RomanNumeralAux (abs x)
    | otherwise = Just $ integral2RomanNumeralAux x 

integral2RomanNumeralAux 0 = Helpers.zero
integral2RomanNumeralAux x = roman ++ (integral2RomanNumeralAux restX)
    where
        (value, roman) = Helpers.findNearestMaxRoman x
        restX = x - value

-- | Translate Roman Numeral String to Integral value (if possible)
romanNumeral2Integral :: (Integral a, Show a) => String -> Maybe a
romanNumeral2Integral roman  
    | roman == Helpers.zero = Just 0
    | head roman == Helpers.negativePrefix = fmap negate $ romanNumeral2IntegralPositive $ tail roman 
    | otherwise = romanNumeral2IntegralPositive roman

romanNumeral2IntegralPositive roman = if ((RomanNumeral roman) == (pack (fromMaybe 0 res))) then res else Nothing
    where 
        res = validateNumeralRange $ romanNumeral2IntegralAux roman


validateNumeralRange :: (Integral a, Show a) => Maybe a -> Maybe a
validateNumeralRange Nothing = Nothing
validateNumeralRange (Just romanInt)
    | romanInt < fromIntegral Helpers.minimal || romanInt > fromIntegral Helpers.maximal = Nothing
    | otherwise = (Just romanInt)
            
romanNumeral2IntegralAux (x:y:rest) = case lookup [x, y] Helpers.charToInt of
    Just v -> fmap ((fromIntegral v) +) (romanNumeral2IntegralAux rest)
    Nothing -> romanNumeral2IntegralAuxSingle (x:y:rest)

romanNumeral2IntegralAux (x:rest) = romanNumeral2IntegralAuxSingle (x:rest)

romanNumeral2IntegralAux _ = Just 0

romanNumeral2IntegralAuxSingle (x:rest) = case lookup [x] Helpers.charToInt of
        Just v -> fmap ((fromIntegral v) +) (romanNumeral2IntegralAux rest)
        Nothing -> Nothing

-- RomanNumeral instances of Bounde, Num, Ord, Eq, Enum, Real, and Integral
instance Bounded RomanNumeral where
    minBound = pack Helpers.minimal
    maxBound = pack Helpers.maximal

instance Eq RomanNumeral where
    (==) (RomanNumeral a) (RomanNumeral b) = a == b

instance Ord RomanNumeral where
    compare r1 r2 = compare (unpack r1) (unpack r2)

instance Num RomanNumeral where
    (+) r1 r2 = pack $ ( (unpack r1) + (unpack r2) )
    (*) r1 r2 = pack $ ( (unpack r1) * (unpack r2) )
    negate roman = pack $ negate $ unpack roman
    abs roman = pack $ abs $ unpack roman
    signum roman = pack $ signum $ unpack roman
    fromInteger num = pack num

instance Enum RomanNumeral where
    toEnum num = pack num
    fromEnum roman = fromInteger $ unpack roman

instance Real RomanNumeral where
    toRational roman = toRational $ unpack roman

instance Integral RomanNumeral where
    quotRem r1 r2 = (pack x1, pack x2)    
        where
            (x1, x2) = quotRem (unpack r1) (unpack r2)

    toInteger = toInteger . unpack
