module BinarySearchTree where

import qualified Data.List
-- You might want to use some externals. Better use qualified import
-- so there won't be any clash
-- for example instead of "sort" (from Data.List) use "Data.List.sort"

-- !! DO NOT CHANGE BSTree data type and type signatures of functions

-- | Binary search tree as described at wikipedia:
--  https://en.wikipedia.org/wiki/Binary_search_tree
data BSTree a = Node a (BSTree a) (BSTree a)
              | Nil
              deriving (Show, Read, Eq)

value :: BSTree a -> a
value Nil = error "Nil does not have a value"
value (Node x _ _) = x

left :: BSTree a -> BSTree a
left Nil = Nil
left (Node _ l _) = l

right :: BSTree a -> BSTree a
right Nil = Nil
right (Node _ _ r) = r

-- HELPERS
isUnique :: Ord a => [a] -> Bool
isUnique lst = length lst == length (Data.List.nub lst)

isSorted :: Ord a => [a] -> Bool
isSorted lst = lst == Data.List.sort lst

-- | Check whether is @BSTree@ valid (i.e., does not violate any rule)
-- TODO: implement validity check
isValid :: Ord a => BSTree a -> Bool
isValid tree = isValidAsList (toList tree)

isValidAsList :: Ord a => [a] -> Bool
isValidAsList lstTree = isSorted lstTree && isUnique lstTree
-- | Check whether is @BSTree@ is leaf
-- TODO: implement leaf check
isLeaf :: Ord a => BSTree a -> Bool
isLeaf tree = 
    tree /= Nil && 
    left tree == Nil &&
    right tree == Nil

-- OTHER SOLUTION :)
-- isLeaf (Node _ Nil Nil) = True
-- isLeaf _ = False

-- | Count all nodes in @BSTree@
-- TODO: implement counting all nodes of the tree
size :: BSTree a -> Integer
size Nil = 0
size tree = 1 + size (left tree) + size (right tree)

-- OTHER SOLUTION :)
-- size Nil = 0
-- size (Node _ left right) = 1 + size left + size right

-- | Height of @BSTree@ (height of @Nil@ is 0)
-- TODO: implement finding out height of the tree
height :: BSTree a -> Integer
height Nil = 0
height tree = 1 + max (height (left tree)) (height (right tree))

-- OTHER SOLUTION :)
-- height Nil = 0
-- height (Node _ left right) = 1 + max (height left) (height right)

-- | Minimal height in the @BSTree@ (height of @Nil@ is 0)
-- TODO: implement finding out minimal depth of the tree
minHeight :: BSTree a -> Integer
minHeight Nil = 0
minHeight tree = 1 + min (minHeight (left tree)) (minHeight (right tree))

-- OTHER SOLUTION :)
-- height Nil = 0
-- height (Node _ left right) = 1 + min (minHeight left) (minHeight right)

-- | Check if given element is in the @BSTree@
-- TODO: implement finding out if element is in the tree
contains :: Ord a => BSTree a -> a -> Bool
contains Nil _ = False
contains (Node x left right) y
    | x > y = contains left y
    | x < y = contains right y
    | otherwise = x == y

-- | Create new tree with given element inserted
-- TODO: implement insertion to the tree
insert :: Ord a => BSTree a -> a -> BSTree a
insert Nil x = Node x Nil Nil
insert (Node y left right) x
    | x == y    = Node y left right
    | x < y     = Node y (insert left x) right
    | otherwise = Node y left (insert right x)
    
-- | Create new tree with given element deleted (min element in the right subtree strategy)
-- TODO: implement deletion from the tree
delete :: Ord a => BSTree a -> a -> BSTree a
delete Nil _ = Nil
delete (Node y left right) x  
	| x == y = swapWithMin (Node y left right)
	| x  < y = Node y (delete left x) right
    | x  > y = Node y left (delete right x)

swapWithMin :: Ord a => BSTree a -> BSTree a 
swapWithMin (Node x Nil right) = right
swapWithMin (Node x left Nil) = left
swapWithMin (Node x left right) = (Node minValue left (delete right minValue)) 
	where 
		minValue = minTreeValue right

minTreeValue :: (Ord a) => BSTree a -> a
minTreeValue (Node x Nil _) = x
minTreeValue (Node _ left _) = minTreeValue left

-- | Convert @BSTree@ to list (will be in ascending order if tree is valid)
-- TODO: implement conversion from tree to list
toList :: BSTree a -> [a]
toList Nil = []
toList (Node x left right) = toList left ++ [x] ++ toList right

-- | Build new @BSTree@ from arbitrary list with use of median (left if even)
-- TODO: implement conversion from list to tree, use median (hint: sort)
fromList :: Ord a => [a] -> BSTree a
fromList lst = fromSortedList (Data.List.nub (Data.List.sort lst))

fromSortedList :: Ord a => [a] -> BSTree a
fromSortedList [] = Nil
fromSortedList [x] = Node x Nil Nil
fromSortedList lst = Node x (fromSortedList left) (fromSortedList right)
    where 
        len = (length lst)
        halfLen = len `div` 2
        medianIdx = halfLen - if(len `mod` 2 == 0) then 1 else 0
        x = lst !! medianIdx
        left = take medianIdx lst
        right = drop (medianIdx + 1) lst
